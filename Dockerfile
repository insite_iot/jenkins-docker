FROM jenkins:latest
MAINTAINER cperezv@insite.com.co
USER root

# Install the latest Docker CE binaries
RUN apt-get update && \
    apt-get -y install apt-transport-https \
      ca-certificates \
      curl \
      gnupg2 \
      software-properties-common && \
    curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg > /tmp/dkey; apt-key add /tmp/dkey && \
    add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
      $(lsb_release -cs) \
      stable" && \
   apt-get update && \
   apt-get -y install docker-ce

# Install docker-compose

RUN curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose


# download the last jenkins version
WORKDIR /tmp
RUN wget http://updates.jenkins-ci.org/download/war/2.153/jenkins.war \ 
    && mv jenkins.war /usr/share/jenkins \
    && chown jenkins:jenkins /usr/share/jenkins/jenkins.war

# install pip and aws 

WORKDIR /root

RUN apt-get install python-pip -y \
    && pip install awscli --upgrade --user \
    && echo "export PATH=~/.local/bin:$PATH">>/root/.bashrc 



ENTRYPOINT ["/bin/tini", "--", "/usr/local/bin/jenkins.sh"]
